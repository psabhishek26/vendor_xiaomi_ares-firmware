LOCAL_PATH := $(call my-dir)

# add-radio-file function with support for both .bin and .img files
define add-radio-file
$(call add-radio-file-check-existence,$(1))
$(call add-radio-file-check-existence,$(1:.img=.bin))
endef

ifeq ($(TARGET_DEVICE),ares)

RADIO_FILES := $(wildcard $(LOCAL_PATH)/images/*)
$(foreach f, $(notdir $(RADIO_FILES)), \
    $(call add-radio-file,images/$(f)))

AB_OTA_PARTITIONS += \
    preloader \
    md1img \
    spmfw \
    audio_dsp \
    pi_img \
    dpm \
    scp \
    sspm \
    mcupm \
    cam_vpu1 \
    cam_vpu2 \
    cam_vpu3 \
    gz \
    lk \
    tee \
    mitee
    
endif


